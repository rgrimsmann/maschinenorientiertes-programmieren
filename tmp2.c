#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
int main(void){
	
	int teil = 2;
	int ganzes = 3;
	
	while (scanf("%d", &ganzes) == 0) 
	{
		printf("Nochmal: ");
		int ch =  0; 
		do {
			ch = getchar();
			if (ch == EOF)
				return 1;
		} while (!isspace(ch)); 
	}
		
		
	printf("%d/%d ergibt %d %%\n", teil, ganzes, teil  * 100 / ganzes);
	int promille = teil * 1000 / ganzes;
	printf(" ... ergibt: %d,%d %%\n", promille / 10, promille % 10);
	// int gerundet = (teil * 1000) / ganzes + 1/2
	int gerundet = (teil * 1000 + ganzes / 2 ) / ganzes;
	printf(" ... ergibt: %d,%d %%\n", gerundet / 10, gerundet % 10);
	
	int gerundet2 = (teil * 10000 + ganzes / 2 ) / ganzes;
	printf(" ... ergibt: %d,%.1d %%\n", gerundet2 / 100, gerundet2 % 100);
	
	
	
	
	
	
	
	return 0;
}