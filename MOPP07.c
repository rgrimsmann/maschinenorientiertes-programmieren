#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define ERROR(s) {printf("%s\n", s);}

#define LINE_L 1024
#define NAME_L 14
#define ASCII_OFFSET 32
#define ERRNUM 0xdeadbeaf

typedef struct binTree {
	struct binTree *left;
	struct binTree *right;
	long matrikelnumber;
	char *name;
	char *lastname;
} tBinTree;

typedef enum {
	SORT_NAME,
	SORT_VORNAME,
	SORT_NUMBER
} tSort;


tBinTree* CreateNode(const char* vorname, const char *nachname, long martNr) {
	tBinTree *r = NULL;
	r = malloc(sizeof(tBinTree));
	if(!r) {
		return NULL;
	}
	r->name = calloc(1, strlen(vorname) + 1);
	r->lastname = calloc(1, strlen(nachname) + 1);
	if(!r->name || !r->lastname) {
		free(r->name);
		free(r->lastname);
		free(r);
		return NULL;
	}
	strcpy(r->name, vorname);
	strcpy(r->lastname, nachname);
	r->matrikelnumber = martNr; //Feste gr��e
	r->left = NULL;
	r->right = NULL;
	return r;
}

void PrintTree(tBinTree *t) {
	if(t) {
		PrintTree(t->left);
		printf("%-14s%-14s%d\n", t->name, t->lastname, t->matrikelnumber);
		PrintTree(t->right);
	}
}

void ToUpper(const char* c, char *dest) {
	char mychar = 0;
	for(int i = 0; c[i]; i++) {
		mychar = c[i];
		if(islower(mychar)) {
			dest[i] = (char)(c[i] - ASCII_OFFSET);
		} else {
			dest[i] = c[i];
		}
	}
	dest[strlen(c)] = 0;
}

int Compare(const tBinTree *a, const tBinTree *b, tSort c) {
	if(!a || !b) {
		return ERRNUM;
	} 
	char name_a[LINE_L] = "";
	char name_b[LINE_L] = "";
	switch(c) {
		case SORT_NAME: 
			ToUpper(a->lastname, name_a);
			ToUpper(b->lastname, name_b);
			return strcmp(name_a, name_b);
			break;
		case SORT_VORNAME: 
			ToUpper(a->name, name_a);
			ToUpper(b->name, name_b);
			return strcmp(name_a, name_b);
			break;
		case SORT_NUMBER: 
				return (int)(a->matrikelnumber - b->matrikelnumber);
			break;
		default: 
			break;
	}
}

tBinTree* ReadElement(FILE *f) {
	//char line[LINE_L] = "";
	char name[NAME_L] = "";
	char lastname[NAME_L] = "";
	long mn = 0;
	while(!feof(f)) {
		fscanf(f, "%13s", name);
		fscanf(f, "%13s", lastname);
		fscanf(f, "%13d", &mn);
		return CreateNode(name, lastname, mn);
	}
}

tBinTree* AddNode(tBinTree* root, tBinTree *neu, tSort s) {
	tBinTree *tree = NULL;
	if(!root) {
		return neu;
	} 
	tree = root;
	while(1) {
		int cmp = Compare(tree, neu, s);
		if(cmp == 0) { // Daten gleich
			return NULL;
		} else if(cmp < 0) { //b ist gr��er -> gehe rechts
			if(tree->right) {
				tree = tree->right;
				continue;
			} else {
				tree->right = neu;
				return root;
			}
		} else { //!Rechts
			if(tree->left) {
				tree = tree->left;
			} else {
				tree->left = neu;
				return root;
			}
		}
	}
}

void DeleteTree(tBinTree *root) {
	if (root) {
		DeleteTree(root->left);
		DeleteTree(root->right);
		free(root->name);
		free(root->lastname);
		free(root);
	}
}

tBinTree* ReadFile(const char* filename, tSort s) {
	tBinTree *root = NULL;
	FILE *tf = NULL;
	tf = fopen(filename, "rt");
	if(!tf) {
		ERROR("Datei kann nicht ge�ffnet werden!");
		return NULL;
	}
	while(!feof(tf)) {
		root = AddNode(root, ReadElement(tf), s);	
	}
	fclose(tf);
	return root;
}

int main(void){
	/*tBinTree *t = CreateNode("Robin", "Grimsmann", 1150538);
	t->right = CreateNode("Example", "User", 124567);
	t->left = CreateNode("Test", "Benutzer", 123445);
	t->left->right = CreateNode("Noch", "Einer", 23);
	PrintTree(t);
	DeleteTree(t);
	t = NULL;
	
	
	int c = Compare(mf, mm, SORT_NAME); //MusterFrau steht vor MusterMann 7 zeichen trennen die beiden
	int d = Compare(mf, mm, SORT_NUMBER); // 1 steht vor 2 no shit! 
	int e = Compare(mf, mm, SORT_VORNAME); //Anton ist vier Zeichen von Erika erntfernt
	*/
	printf("Bitte Sortierverfahren Ausw�hlen: \n0 == Name, 1 == Vorname, 2 == Matrikelnummer\n>_ ");
	tSort s = 0;
	tBinTree *rf = NULL;
	scanf("%d", &s);
	if(s >= 0 && s <= 2) {
		printf("%-14s%-14s%s", "Vorname", "Nachname", "Matrikelnummer\n");
		rf = ReadFile("studenten.txt", s);
		PrintTree(rf);
	} else {
		ERROR("Kein Sortierkriterium!");
	}
	DeleteTree(rf);
	rf = NULL;
	return 0;
}