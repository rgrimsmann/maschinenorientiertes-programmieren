#include <stdio.h>
#include <string.h>

#define SIZE 1024
#define WORDSIZE 32

int CountChar(const char* src, char ch) {
	int cnt = 0;
	if(src == NULL || ch == NULL) {
		return -1;
	}
	while(src[0] != 0) {
		if(src[0] == ch) {
			cnt++;
		}
		src++;
	}
	return cnt;
}

/*int IsEmpty(const char* src) {
	if(src[0] == '\n') {
		return 0;
	}
	return 1;
}*/

int ReplaceChar(char* dest, const char* src, char ch, char newCh) {
	int rep = 0;
	int i = 0;
	if(dest == NULL || src == NULL || ch == NULL || newCh == NULL) {
		return -1;
	}
	for(i = 0; src[i] != 0; i++) {
		char c = src[i];
		if(src[i] == ch) {
			rep++;
			dest[i] = newCh;
		} else {
			dest[i] = src[i];
		}
	}
	dest[i] = 0;
	return rep;
}

int CountWord(const char* src, const char* word) {
	int count = 0;
	if(src == NULL || word == NULL) {
		return -1;
	}
	char *res = strstr(src, word);
	while(res != NULL)  {
		count++;
		int strl = strlen(word);
		res += strl;
		res = strstr(res, word);
	}
	return count;
}


int ReplaceWord(char* dest, const char* src, const char* word, const char* newWord) {
	int cnt = 0;
	if(dest == NULL || src == NULL || word == NULL || newWord == NULL || strlen(word) != strlen(newWord)) {
		return -1;
	}
	strcpy(dest, src);
	char *tmp = strstr(dest, word);
	while(tmp != NULL) {
		cnt++;
		strncpy(tmp, newWord, strlen(newWord));
		tmp += strlen(newWord);
		tmp = strstr(tmp, word);
	}
	return cnt;
}
/*Nur erstes Auftreten wird ver�ndert*/
int ReplaceWord2(char* dest, const char* src, const char* word, const char* newWord) {
	if(dest == NULL || src == NULL || word == NULL || newWord == NULL) {
		return -1;
	}
	char *z1 = NULL;
	char *z2 = NULL;
	int s2 = strlen(word);
	int s = strlen(newWord);
	//int countWord = CountWord(src, word);
	int cnt = 0;
	strcpy(dest, src);
	z2 = strstr(src, word); //sollten gleich sein gleicher inhalt
	z1 = strstr(dest, word);
	if(z2 == NULL || z1 == NULL)  //Abbruch wenn wort nicht gefunden wird
		return 0;
	
	//for(int i = 0; i < countWord; i++) {
	z2 += s2;
	z1 += s;
	strcpy(z1, z2); //anzahl der stellen anpassen 
	z2 = strstr(z2, word);
	z1 = strstr(z1, word);
	//}
	z1 = dest;
	char t[WORDSIZE] = "";
	strcpy(t, word);
	if(s2 > s) {
		t[s] = '\0'; //alternativ t[s] = 0;
	}
	z1 = strstr(z1, t);
	//while(z1 != 0) { replace all w�re das hier ohne kommentare
	for(int i = 0; i < s; i++) {
		z1[i] = newWord[i];
	}
	//z1 += s;
	//	z1 = strstr(z1, t);
	//cnt++;
	//}
	return 1;
	
}

int main(void){
	char st[SIZE] = "";
	//char *stp = st;
	char ch = 0; 
	char er = 0;
	char word[WORDSIZE] = "";
	char erword[WORDSIZE] = "";
	char d[SIZE] = "";
	printf("Texteingeben: ");
	fgets(st, sizeof(st), stdin);
	if('\n' == st[0]) { 
		// strcpy(st, "nanana!");
		st[0] = 'n';
		st[1] = 'a';
		st[2] = 'n';
		st[3] = 'a';
		st[4] = 'n';
		st[5] = 'a';
		st[6] = '!';
	}
	printf("Such nach Zeichen: ");
	scanf("%c", &ch);
	if('\n' == ch){
		ch = 'a';
	} else
		getchar();
	printf("Zeichen Ersetzen durch: ");
	scanf("%c", &er);
	if('\n' == er) {
		er = 'o';
	} else 
		getchar();
	int a = CountChar(&st, ch);
	if(-1 != a) {
		printf("Der Buchstabe \"%c\" wurde %d mal gefunden\n\n", ch, a);
	}
	
	ReplaceChar(d, st, ch, er);
	printf("Ersetzter Text: %s\n", d);
	printf("Nach Wort suchen: ");
	scanf("%31s", word);
	a = CountWord(st, word);
	printf("Wort \"%s\" im Text enthalten: %d mal\nErsetze \"%s\" durch: ", word, a, word);
	scanf("%31s", erword);
	if(ReplaceWord(d, st, word, erword) != -1) {
		printf("Neuer Text: %s\n", d);
	} 
	if(ReplaceWord2(d, st, word, erword) != -1) {
		printf("Neuer Text: %s\n", d);
	} /**/
	return 0;
}