#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define ErrLog(s) { printf("Error: %s", s); }
#define STARTSIZE 16
#define MAX 1024

/**
	Fragen: Pfeiloperator zum zugriff auf struktur elemente
	tList *n = ....
	n->next = "";
	-Struktur muss aussehen wie meine, such die passende beschreibung 
	-allocieren von Speicher im heap: malloc anweisung muss so aussehen!
	tList *n = malloc(strlen(str) + sizeof(tList));
	-L�schen von Listen: 
	free(n);
*/
typedef struct list { 
	struct list *next; 
	char word[1]; //muss unten stehen
} tList;

tList* CreateWord(char *str) {
	if(str == NULL) 
		return NULL;
	tList *n = malloc(strlen(str) + sizeof(tList));
	strcpy(n->word, str);
	n->next = NULL;
	return n;
}

tList* AddElement(tList *head, char *new) { /*F�gt am Ende einer bestehenden Liste ein*/
	if(!head) {
		return CreateWord(new);
	} 
	tList *h = head;
	while(h->next) {
		h = h->next;
	}
	h->next = CreateWord(new);
	return h->next; // gibt den Fu� zur�ck
}

tList* AddElementTop(tList *head, char *w ) {
	if(!head) {
		return CreateWord(w);
	} 
	tList *t = CreateWord(w);
	t->next = head;
	return t;
}

void PrintList(tList* head) {
	tList *index = head;
	while(index) {
		printf("%s ", index->word);
		index = index->next;
	}
	printf("\n");
}

tList* CopyReverse(tList* l) {
	tList *tmp = l;
	tList *n = NULL;
	while(tmp) {
		n = AddElementTop(n, tmp->word);
		tmp = tmp->next;
	}
	return n;
}

void DeleteList(tList *head) {
	while(head) {
		tList *t = head;
		head = head->next;
		free(t);
		t = NULL;
	}
}

tList* ReadText(void) {
	char input[MAX] = "";
	scanf("%1023s", input);
	tList* head = CreateWord(input);
	while(input[0] != '.'){
		scanf("%s", input);
		AddElement(head, input);
	}
	//fflush(stdin);
	return head;
}

tList* InReplaceWord(tList* head, char* w, char* nw){
	if (head == NULL || w == NULL || nw == NULL){
		 return NULL;
	} 
	tList *t = head;
	tList *p = NULL;
	tList *n = NULL;
	while(t) {
		if(strcmp(t->word, w) == 0) {
			n = CreateWord(nw);
			n->next = t->next;
			free(t);
			t = n;
			if(p != NULL) {
				p->next = n;
			} else {
				head = n;
			}
		}
		p = t;
		t = t->next;
	}
	return head;
}

int main(void){
	tList *head = NULL;
 	printf("Text eingeben: ");
	head = ReadText();
	PrintList(head);
	printf("Suche nach: ");
	char w[MAX] = "";
	char nw[1024] = "";
	scanf("%1023s", w);
	//fflush(stdin);
	printf("Ersetze Durch: ");
	scanf("%1023s", nw);
	//fflush(stdin);
	head = InReplaceWord(head, w, nw);
	PrintList(head);
	tList *head2 = CopyReverse(head);
	PrintList(head2);
	DeleteList(head);
	head = NULL;
	DeleteList(head2);
	head2 = NULL;
	return 0;
}