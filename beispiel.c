/* Erstes Beispiel Maschinenorientiertes Programmieren */
// C++-Kommentar m�glich ab C99
#define MAX_EINGABE 12
#include <stdio.h>

long Fakultaet(int n);

int main(void) {

	int n = 0;
	
	printf("Berechnung der Fakultaet\n" 
	       "Geben Sie eine Zahl zwischen %d und %d ein:",
	        1, MAX_EINGABE);
	scanf("%d", &n);
	
	if (n > MAX_EINGABE) {
		printf("Fehler: Eingabe zu gross!\n");
		printf("Nochmal!");
	} else
		printf("Die Fakultaet von %d ist %ld\n", n, Fakultaet(n));
	
	return 0;
} 

long Fakultaet(int n) {
	long f = 1;

	for (int i = 1; 
		i <= n; 
		i++) 
		f *= i;
	
	return f;
}



















