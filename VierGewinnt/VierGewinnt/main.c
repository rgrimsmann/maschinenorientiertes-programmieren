#include "playground.h"
#include <stdio.h>

#define MAX_X 4
#define MAX_Y 4

int DropChip(tPlayer f[], int col, tPlayer chip) {
	if(f[MAX_X * (MAX_Y - 1) + col] != EMPTY) {
		return 1;
	} else {
		f[MAX_X * (MAX_Y -1) + col] = chip;
		DrawPlayGround(f, MAX_X, MAX_Y);
		for(int i = MAX_Y - 2; i >= 0 ; i--) {
			if(f[MAX_X * i + col] == EMPTY) {
				f[MAX_X * (i + 1) + col] = EMPTY;
				f[MAX_X * i + col] = chip;
				DrawPlayGround(f, MAX_X, MAX_Y);
			}else {
				break;
			}
		}
		return 0;
	}
}

int CheckComplete(const tPlayer array[]) {
	for(int x = (MAX_X - 1) * (MAX_Y - 1); x < (MAX_X) * (MAX_Y); x++) {
		if(array[x] == EMPTY) {
			return 0;
		}
	}
	return 1;
}

int CheckHor(const tPlayer f[], tPlayer p, int *x, int *y) {
	for(int row = 0; row <= MAX_Y; row++) {
		for(int col = 0; col <= MAX_X - 4; col++) { /* while(col <= MAX_X - 4) */
			if(f[MAX_X * row + col] == p) {
				
				int i = 0;
				while (i < 4) {
					if(f[MAX_X * row + col + i] != p) {
						break;
					}
					if(i == 3) {
						*x = col;
						*y = row; 
						return 1;
					}
					i++;
				}
				/*for(int i = 0; i < 4; i++) {
					if(f[MAX_X * row + col + i] != p) {
						break;
					}
					if(i == 3) {
						*x = col;
						*y = row; 
						return 1;
					}
					
				}*/
			}
		} 
	}
	return 0;
}

int CheckVer(const tPlayer f[], tPlayer p, int *x, int *y) {
	for(int row = 0; row <= MAX_Y; row++) {
		for(int col = 0; col <= MAX_X - 4; col++) {
			if(f[MAX_X * row + col] == p) {
				for(int i = 0; i < 4; i++) {
					if(f[MAX_X * (row + i) + col ] != p) {
						break;
					}
					if(i == 3) {
						*x = col;
						*y = row; 
						return 1;
					}
					
				}
			}
		} 
	}
	return 0;
}

int CheckDiag(const tPlayer f[], tPlayer p, int *x, int *y) {
	for(int row = 0; row <= MAX_Y; row++) {
		for(int col = 0; col <= MAX_X - 3; col++) {
			if(f[MAX_X * row + col] == p) {
				for(int i = 0; i < 4; i++) {
					if(i == 3) {
						return 1;
					}					
				}
			}
		}
	}
}

void SetField(tPlayer *field, tPlayer chip) {
	for(int i = 0; i < MAX_X * MAX_Y; i++) {
		field = chip;
	}
}

int main(int argc, char **argv){
	int row = 0;
	int col = 0;
	
	int amount = MAX_Y * MAX_X;
	if(amount % 2 == 1) {
		amount++;
	}
	amount /= 2;
	int player1 = amount;
	int player2 = amount;
	
	InitPlayGround(MAX_X, MAX_Y);
	
	//Aufgabe 1
	tPlayer field[MAX_X * MAX_Y] = {EMPTY};
	SetField(&field, EMPTY);
	DrawPlayGround(field, MAX_X, MAX_Y);
	
	/*tPlayer t[MAX_X * MAX_Y] = {0, 0, 0, 0, 0, 0, 0, 
								0, 0, 0, 0, 0, 0, 0, 
								0, 2, 2, 2, 2, 0, 0,
								0, 0, 2, 1, 1, 1, 1, 
								0, 0, 2, 1, 0, 0, 0,
								0, 0, 2, 1, 0, 0, 0,
								0, 0, 2, 1, 0, 0, 0};
	int result = CheckVer(t, PLAYER_R, &row, &col);
	result += CheckHor(t, PLAYER_L, &row, &col); 
	result += CheckHor(t, PLAYER_R, &row, &col);
	result += CheckVer(t, PLAYER_L, &row, &col); 
	
	result += CheckHor((tPlayer[MAX_X*MAX_Y]){
	PLAYER_R, PLAYER_R, PLAYER_R, PLAYER_R }, PLAYER_R, &row, &col);
	result += CheckHor((tPlayer[MAX_X*MAX_Y]){
	0, 0, PLAYER_R, 0, PLAYER_R, PLAYER_R, PLAYER_R, 
	PLAYER_R, PLAYER_R, 0, PLAYER_R}, PLAYER_R, &row, &col);*/
	
	//Aufgabe 2 + 3 
	while(!CheckComplete(field)) {
		DrawChips(player1, player2, PLAYER_R);
		if(DropChip(field, SelectCol(), PLAYER_R) == 0) {
			player1--;
		}
		//Check Routine
		if(CheckHor(field, PLAYER_R, &row, &col) == 1) {
			HighlightChipHor(row, col);
			ShowYouWon(PLAYER_R);
			break;
		} 
		if(CheckVer(field, PLAYER_R, &row, &col) == 1) {
			HighlightChipVer(row, col);
			ShowYouWon(PLAYER_R);
			break;
		}
		
		DrawChips(player1, player2, PLAYER_L);
		if(DropChip(field, SelectCol(), PLAYER_L) == 0) {
			player2--;
		}
		if(CheckHor(field, PLAYER_L, &row, &col) == 1) {
			HighlightChipHor(row, col);
			ShowYouWon(PLAYER_L);
			break;
		} 
		if(CheckVer(field, PLAYER_L, &row, &col) == 1) {
			HighlightChipVer(row, col);
			ShowYouWon(PLAYER_L);
			break;
		}
		if(player2 == 0 || player1 == 0) {
			break;
		}
	}
	//DrawChips(amount, amount, PLAYER_L);
	return 0;
}