#include <stdio.h>

typedef struct {
	int x;
	int y;
} tPoint;

typedef struct {
	unsigned char red, green, blue;
} tColor;

typedef struct {
	tPoint p;
	tColor c;
} tPixel;

void DrawPixel(tPixel *t) {
	printf("(%d | %d)", t->p.x, t->p.y);
}

int main(void){
	tPixel c = {{0}, {255}};	
	tPixel *a = &c;
	a->p.x = 111;
	DrawPixel(&c);
	return 0;
}