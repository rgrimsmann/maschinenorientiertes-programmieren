#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define GENAUIGKEIT 1000
#define MAX_EINGABE 30

long FibRekursiv(int n);
unsigned int Goldenratio(unsigned int n);
int OptimumLength(int n, int b);

int main(void){
	int n = 0; 
	int b = 0;
	int bb = 0;
	int phi = 0;
		
	printf("Berechnung von Fibionacci-Zahlen und dem goldenen Schnitt\nBitte n eingeben: ");
	if(scanf("%d", &n) == 0 || n <= 0 || n > MAX_EINGABE) {
		printf("Die Eingabe entspricht nicht den Anforderungen!");
		return 1;
	}
	//while(scanf("%d", &n) == 0 || n <= 0 || n > MAX_EINGABE){
	//	printf("Nochmal:");
	//	int ch =  0; 
	//	do {
	//		ch = getchar();
	//		if (ch == EOF)
	//			return 1;
	//	} while (!isspace(ch)); 
	//}
	
	printf("Fibionacci-Zahlen von %d ist %ld\n", n, FibRekursiv(n));
	phi = Goldenratio(n);
	printf("Der goldene Schnitt f�r n=%d betr�gt: %d,%03d\n", n, phi / GENAUIGKEIT, phi % GENAUIGKEIT);
	
	printf("Breite des Fusballfeldes eingeben: ");
	if(!(scanf("%u,%u", &b, &bb) == 2) || bb < 0 || bb > 9 || b <= 0){
		printf("Ihre Eingabe entspricht nicht den Anforderungen!");
		return 1;
	}
	
	//while(scanf("%u,%u", &b, &bb) == 0 || (b <= 0 && bb > 9) || b <= 0){
	//	printf("Nochmal:");
	//	int ch =  0; 
	//	do {
	//		ch = getchar();
	//		if (ch == EOF)
	//			return 1;
	//	} while (!isspace(ch)); 
	//} 
	
	unsigned int gr = b * 10 + bb; 
	int l = OptimumLength(n, gr);
			printf("Die ideale L�nge bei %d,%d m betr�gt: %d,%02d", b, bb, l / (100), l % (100));
	return 0;
}

long FibRekursiv(int n) {
	if(n == 0 || n < 0) {
		return 0;
	} else if(n == 1) {
		return 1;
	} else {
		return FibRekursiv(n - 2) + FibRekursiv(n - 1);
	}
}

unsigned int Goldenratio(int n) {
	if(n < 2) 
		return 0;
	unsigned int teile = FibRekursiv(n);
	unsigned int ganzes = FibRekursiv(n - 1);
	return  (teile * GENAUIGKEIT + ganzes / 2)/ ganzes;
}

int OptimumLength(int n, int b) {
	return ((Goldenratio(n) * b) / 100);
}