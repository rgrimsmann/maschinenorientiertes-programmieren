#include <stdio.h>
#include <math.h>

float CountCentsFloat(unsigned int betrag) {
	float f = 0.0f;
	for(unsigned int i = 0; i < betrag * 100; i++) {
		f += 0.01f;
	}
	return f;
}

double CountCentsDouble(unsigned int betrag) {
	double f = 0.0f;
	for( unsigned int i = 0; i < betrag * 100; i++ ) {
		f += 0.01;
	}
	return f;
}

float CalcFloatInterest(float amount, float intInPercent, int days) {
	float cpyamt = amount;
	for ( int i = 0; i < days; i++ ) {
		amount *= (1.0 + (intInPercent/36500.0f));
	}
	return amount - cpyamt;
}

double CalcDoubleInterest(double amount, double intInPercent, int days) {
	double cpyamt = amount;
	for ( int i = 0; i < days; i++ ) {
		amount *= (1.0 + (intInPercent/36500.0));
	}
	return amount - cpyamt;
}

int CalcSaveTimeFloat(float amount, float intInPercent) {
	int days = 0;
	float cents = 0.0f;
	
	while(cents < amount) {
		cents += 0.01;
		cents *= (1.0 + (intInPercent/36500.0)); 
		days++;
	}
	return days;
}

int CalcSaveTimeDouble(double amount, double intInPercent) {
	int days = 0;
	double cents = 0.0;
	while(cents < amount) {
		cents += 0.01;
		cents *= (1.0 + (intInPercent/36500.0)); 
		days++;
	}
	return days;
}

int main(void){
	unsigned int n = 0;
	float z = 0.0;
	int lz = 0;
	float s = 0.0;
	double j = 0.01;
	printf("Bitte Betrag eingeben: ");
	scanf("%d", &n); //100 passt auch nicht, wenn man alle stellen betrachte
	printf("Der betrag in Cent ergibt: %.2f\n", CountCentsFloat(n));
	printf("Der betrag in Cent ergibt: %.2lf\n", CountCentsDouble(n));
	printf("Bitte Zinsatz in %% eingeben: ");
	scanf("%f", &z);
	printf("Laufzeit in Tagen eingeben: ");
	scanf("%d", &lz);
	s = CalcFloatInterest((float)n, z, lz);
	printf("Die Zinsen mit float betragen: %.2f\n", s);
	j = CalcDoubleInterest(CountCentsDouble(n), z, lz);
	printf("Die Zinsen mit double betragen: %.2lf\n", j);
	
	int savedays = CalcSaveTimeFloat((float)n, z);
	printf("Tage zu Sparen: %d", savedays);
	savedays = CalcSaveTimeDouble((double)n, (double)z);
	printf("Tage zu Sparen: %d", savedays);
	return 0;
}